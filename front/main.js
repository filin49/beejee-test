import jQuery from 'jquery';
window.jQuery = window.$ = jQuery;
require('bootstrap');
import Vue from 'vue';
window.Swal = require('sweetalert2');
let axios = window.axios = require('axios');
import router from './router';
import App from './App.vue';
import 'bootstrap/dist/css/bootstrap.min.css'
import store from './store/index';
Vue.config.productionTip = false;

axios.defaults.validateStatus = (status) => {
  if (status === 401 && !['home', 'login', 'add'].includes(router.currentRoute.name)) {
    store().dispatch('base/initApp');
    router.push({name: 'home'});
    return true;
  }
  return status >= 200 && status < 300;
};

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    this.$store.dispatch('base/initApp');
  }
}).$mount('#app');
