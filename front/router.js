import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./components/TaskList')
    },
    {
      path: '/test',
      name: 'test',
      component: () => import('./components/TaskList')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./components/Login')
    },
    {
      path: '/logout',
      name: 'logout',
      component: () => import('./components/Logout')
    },
    {
      path: '/add',
      name: 'task-add',
      component: () => import('./components/TaskForm')
    },
    {
      path: '/edit/:id',
      name: 'task-edit',
      component: () => import('./components/TaskForm')
    }
  ]
});