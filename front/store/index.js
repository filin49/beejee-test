import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import base from './modules/base';

const debug = process.env.NODE_ENV !== 'production';

let store;

const initStore = () => {
  return store || (store = new Vuex.Store({
    modules: {
      base,
    },
    strict: debug
  }))
};

export default initStore