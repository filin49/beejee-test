export default {
  namespaced: true,
  state: {
    user: false,
    tasks: [],
    totalPages: 1,
    currentPage: 1,
    orderBy: '',
    orderAsc: false
  },
  mutations: {
    setUser(state, {val}) {
      state.user = val;
    },
    setTotalPages(state, val) {
      state.totalPages = val;
    },
    setCurrentPage(state, val) {
      state.currentPage = val;
    },
    setTasks(state, {val}) {
      state.tasks = val;
    },
    addTask(state, {val}) {
      state.tasks.push(val);
    },
    updateTask(state, {id, val}) {
      state.tasks[id] = val;
    },
    setVal(state, {key, val}) {
      state[key] = val;
    },
  },
  actions: {
    // Инициализация приложения
    initApp({commit, state}) {
      console.log('init app');
      window.axios.post('/index/init', {
        page: state.currentPage,
        orderBy: state.orderBy,
        orderAsc: state.orderAsc,
      }).then(response => {
        commit('setUser', {val: response.data.user});
        commit('setTasks', {val: response.data.tasks});
        commit('setTotalPages', response.data.totalPages);
        if (state.currentPage > state.totalPages) {
          commit('setCurrentPage', state.totalPages);
        }
        if (response.data.user === true) {
          commit('setUser', {val: response.data.user});
        }
      }).catch(e => {
        console.log(e)
      });
    }
  },
  getters: {
    isAuth(state) {
      return state.user ? 1 : 0;
    }
  }
}