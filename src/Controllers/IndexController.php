<?php

namespace BeeJee\Controllers;

use BeeJee\Auth;
use BeeJee\Models\Task;

class IndexController extends Controller
{
    public function getIndex()
    {
        return 'ok from index action';
    }

    /**
     * Инициализация данных
     * @return array
     */
    public function postInit(): array
    {
        $page = $this->getParam('page', 1);
        $totalPage = Task::pageCount();
        if ($page > $totalPage) {
            $page = $totalPage;
        }
        return [
            'user' => Auth::isAuth(),
            'totalPages' => $totalPage,
            'tasks' => Task::getByPage(
                $page,
                $this->getParam('orderBy', ''),
                $this->getParam('orderAsc', false)
            )
        ];
    }
}