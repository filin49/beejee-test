<?php

namespace BeeJee\Controllers;

class Controller
{
    protected $payload = [];

    public function __construct()
    {
        $payload = file_get_contents('php://input');
        $this->payload = json_decode($payload, true);
    }

    /**
     * Получить параметр POST запроса
     * @param string $name
     * @param null $default
     * @return mixed|null
     */
    protected function getParam(string $name, $default = null)
    {
        return isset($this->payload[$name]) ? $this->payload[$name] : $default;
    }
}