<?php

namespace BeeJee\Controllers;

use BeeJee\Auth;
use BeeJee\Exceptions\AuthException;
use BeeJee\Models\Task;

class TaskController extends Controller
{

    /**
     * Переключить статус задачи
     * @return bool
     */
    public function postStatus()
    {
        $task = Task::get($this->getParam('id'));
        Task::update($task['id'], [
            'is_complete' => !$task['is_complete'],
        ]);

        return true;
    }

    /**
     * Сохранить задачу
     * @return bool
     */
    public function postAdd()
    {
        $id = $this->getParam('id');
        if ($id) { // Редактирование?
            if (!Auth::isAuth()) {
                throw new AuthException('Необходима авторизация');
            }
            Task::update($id, [
                'description' => $this->getParam('description', ''),
                'is_edit' => true
            ]);
        } else { // Добавление...
            Task::add([
                'username' => $this->getParam('username'),
                'email' => $this->getParam('email'),
                'description' => $this->getParam('description'),
            ]);
        }

        return true;
    }

    /**
     * Данные задачи
     * @return array
     */
    public function postGet(): array
    {
        $task = Task::get($this->getParam('id'));
        return [
            'task' => $task
        ];
    }

    /**
     * Вход в систему
     * @return bool
     * @throws \BeeJee\Exceptions\ErrorException
     */
    public function postLogin(): bool
    {
        return Auth::login($this->getParam('login'), $this->getParam('passwd'));
    }

}