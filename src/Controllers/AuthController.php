<?php

namespace BeeJee\Controllers;

use BeeJee\Auth;

class AuthController extends Controller
{
    /**
     * Вход в систему
     * @return bool
     * @throws \BeeJee\Exceptions\ErrorException
     */
    public function postLogin()
    {
        return Auth::login($this->getParam('login'), $this->getParam('passwd'));
    }

    /**
     * Выход из системы
     * @return bool
     * @throws \BeeJee\Exceptions\ErrorException
     */
    public function postLogout()
    {
        return Auth::logout();
    }
}