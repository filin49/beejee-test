<?php

namespace BeeJee;


use BeeJee\Exceptions\AuthException;
use BeeJee\Exceptions\ErrorException;

class Kernel {
    private $router;
    public function __construct(Router $router = null)
    {
        $this->router = $router ?? new Router();
    }

    public function handle()
    {
        header('Content-Type: application/json');
        try {
            $result = $this->router->handle();
            if (is_bool($result)) {
                $result = [
                    'status' => $result
                ];
            }
            echo json_encode($result);
        } catch (ErrorException $e) {
            echo json_encode([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        } catch (AuthException $e) {
            http_response_code(401);
            echo json_encode([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}