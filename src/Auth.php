<?php

namespace BeeJee;

use BeeJee\Exceptions\ErrorException;

class Auth
{
    /**
     * Пользователь авторизован?
     * @return bool
     */
    public static function isAuth(): bool
    {
        if (session_status() === 1) {
            session_start();
        }
        if (!empty($_SESSION['USER'])) {
            return true;
        }
        return false;
    }

    /**
     * Авторизация
     * @param string $login
     * @param string $pass
     * @return bool
     * @throws ErrorException
     */
    public static function login(string $login, string $pass): bool
    {
        if (self::isAuth()) {
            throw new ErrorException('Already logged');
        }
        if ($login !== 'admin' || $pass !== '123') {
            return false;
        }
        $_SESSION['USER'] = $login;
        return true;
    }

    /**
     * Выход из системы
     * @return bool
     * @throws ErrorException
     */
    public static function logout(): bool
    {
        if (!self::isAuth()) {
            throw new ErrorException('Not logged');
        }
        unset($_SESSION['USER']);
        return true;
    }
}