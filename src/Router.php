<?php

namespace BeeJee;

use BeeJee\Exceptions\AuthException;
use BeeJee\Exceptions\ErrorException;

class Router
{
    protected $uri;
    protected $nameSpace = '\BeeJee\Controllers\\';
    protected $className = 'IndexController';
    protected $action = 'Index';
    protected $method = 'get';
    protected $controller;

    public function __construct()
    {
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
        $uri = array_filter(explode('/', $_SERVER['REQUEST_URI']));
        $uri = array_map(function($item) {
            return ucfirst(strtolower($item));
        }, $uri);
        if (count($uri) > 1) {
            $this->action = array_pop($uri);
            $this->className = array_pop($uri) . 'Controller';
            if ($uri) {
                $this->nameSpace = '\BeeJee\Controllers\\' . implode('\\', $uri) . '\\';
            }
        } elseif (count($uri) === 1) {
            $this->action = $uri[1];
        }
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getNameSpace(): string
    {
        return $this->nameSpace;
    }

    public function getClassName(): string
    {
        return $this->className;
    }

    public function getMethodName(): string
    {
        return $this->method;
    }

    /**
     * Проверить, что контроллер и метод существуют
     * @return bool
     */
    public function checkExists(): bool
    {
        if (class_exists($this->nameSpace . $this->className)) {
            $name = $this->nameSpace . $this->className;
            $this->controller = new $name;
            if (method_exists($this->controller, $this->method . $this->action)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Проверить права доступа
     * @return bool
     */
    public function checkPerms(): bool
    {
        if (Auth::isAuth()) {
            return true;
        }
        $guestPaths = [
            'AuthController' => 'Login',
            'TaskController' => 'Add',
            'IndexController' => 'Init'
        ];
        return (isset($guestPaths[$this->className]) && $guestPaths[$this->className] === $this->action);
    }

    /**
     * Обработать запрос
     * @return mixed
     * @throws ErrorException
     * @throws AuthException
     */
    public function handle()
    {
        if (!$this->checkExists()) {
            throw new ErrorException('404 Not found');
        }
        if (!$this->checkPerms()) {
            throw new AuthException('Unauthorized');
        }

        return $this->controller->{$this->method . $this->action}();
    }
}