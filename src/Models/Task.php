<?php

namespace BeeJee\Models;


class Task extends Model
{
    protected static $peerPage = 3;

    /**
     * Добавить задачу
     * @param array $payload
     * @return bool
     */
    public static function add(array $payload): bool
    {
        self::db()->insert('tasks', $payload);
        if (self::db()->id()) {
            return true;
        }
        return false;
    }

    /**
     * Обновить задачу
     * @param int $id
     * @param array $payload
     * @return bool|\PDOStatement
     */
    public static function update(int $id, array $payload)
    {
        return self::db()->update('tasks', $payload, ['id' => $id]);
    }

    /**
     * Получить задачу по id
     * @param int $id
     * @return array
     */
    public static function get(int $id): array
    {
        return self::db()->get('tasks', '*', [
            'id' => $id
        ]);
    }

    /**
     * Посчитать количество страниц
     * @return int
     */
    public static function pageCount(): int
    {
        $reordsCount = self::db()->count('tasks');
        $pages = $reordsCount / self::$peerPage;
        $pages += ($reordsCount % self::$peerPage > 0) ? 1 : 0;
        return $pages;
    }

    /**
     * Получить задачи по номеру страницы с учетом сортировки
     * @param int $page
     * @param string $orderBy
     * @param bool $orderAsc
     * @return array|bool
     */
    public static function getByPage(int $page, string $orderBy, bool $orderAsc)
    {
        $offset = ($page - 1) * self::$peerPage;
        $order = ['id' => 'ASC'];
        if ($orderBy) {
            $order = [$orderBy => $orderAsc ? 'ASC' : 'DESC'];
        }
        $result = self::db()->select('tasks', '*', [
            "ORDER" => $order,
            "LIMIT" => [$offset, 3]
        ]);
        return $result ?? [];
    }
}