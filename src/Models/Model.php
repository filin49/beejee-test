<?php

namespace BeeJee\Models;

use Medoo\Medoo;

class Model
{
    private static $conn;

    protected static function db(): Medoo
    {
        if (!self::$conn) {
            self::$conn = new Medoo([
                'database_type' => 'mysql',
                'database_name' => 'beejee',
                'server' => 'db',
                'username' => 'root',
                'password' => 'root'
            ]);
        }
        return self::$conn;
    }
}