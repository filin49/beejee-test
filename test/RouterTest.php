<?php

namespace BeeJee\Test;

use BeeJee\Router;
use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase
{
    /**
     * Проверить корректность разбора uri
     */
    public function testParseUri()
    {
        $_SERVER['REQUEST_URI'] = '/';
        $_SERVER['REQUEST_METHOD'] = 'POST';
        $router = new Router();
        $this->assertEquals('Index', $router->getAction());
        $this->assertEquals('IndexController', $router->getClassName());
        $this->assertEquals('\BeeJee\Controllers\\', $router->getNameSpace());
        $this->assertEquals('post', $router->getMethodName());
        $this->assertFalse($router->checkExists());

        $_SERVER['REQUEST_URI'] = '/index/';
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $router = new Router();
        $this->assertEquals('Index', $router->getAction());
        $this->assertEquals('IndexController', $router->getClassName());
        $this->assertEquals('\BeeJee\Controllers\\', $router->getNameSpace());
        $this->assertEquals('get', $router->getMethodName());
        $this->assertTrue($router->checkExists());

        $_SERVER['REQUEST_URI'] = '/controLLer/actIOn';
        $router = new Router();
        $this->assertEquals('Action', $router->getAction());
        $this->assertEquals('ControllerController', $router->getClassName());
        $this->assertEquals('\BeeJee\Controllers\\', $router->getNameSpace());
        $this->assertFalse($router->checkExists());

        $_SERVER['REQUEST_URI'] = '/folDer/controLLer/actIOn';
        $router = new Router();
        $this->assertEquals('Action', $router->getAction());
        $this->assertEquals('ControllerController', $router->getClassName());
        $this->assertEquals('\BeeJee\Controllers\\Folder\\', $router->getNameSpace());
    }
}