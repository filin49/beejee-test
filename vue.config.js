const path = require("path");

module.exports = {
  // pages: {
  //   index
  // },
  chainWebpack: config => {
    config
        .entry("app")
        .clear()
        .add("./front/main.js")
        .end();
    config.resolve.alias
        .set("@", path.join(__dirname, "./front"));
    config.plugins.has('copy') && config.plugin('copy')
        .tap(([pathConfigs]) => {
          pathConfigs[0].from = path.join(__dirname, './assets');
          return [pathConfigs]
        })
    config.plugin('html').tap(args => {
      args[0].template = path.join(__dirname, '/front/index.html');
      return args;
    });
  },
  // assetsDir: "assets/",
  // publicDir: path.join(__dirname, "./assets"),
  outputDir: path.join(__dirname, "./public/assets"),
  indexPath: path.join(__dirname, "./public/index.html"),
  publicPath: '/assets/',
  devServer: {
    proxy: {
      '/': {
        target: 'http://localhost:80'
      }
    }
  }
};
