<?php

require "../vendor/autoload.php";

define('APP_PATH', dirname(__DIR__));
$kernel = new \BeeJee\Kernel();
$kernel->handle();
